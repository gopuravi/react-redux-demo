import React from 'react';
import Items from './Container/Items/items'
import './App.css';

function App() {
  return (
    <div className="App">
      <Items  />
    </div>
  );
}

export default App;
