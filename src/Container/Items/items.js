import React, { useEffect, useMemo, useCallback } from 'react';
import {connect} from 'react-redux';
import  * as actions from '../../store/action/index';
import ItemList from '../../components/Items/ItemList/ItemList' 
import ItemForm from '../../components/Items/ItemForm/ItemForm'

const Items =(props) =>{
  
   
  useEffect(()=>{
    console.log("Inside Use Effect", props.itemList)
  }); 

  const removeItemHandler = (itemId) =>{
    props.onRemoveItems(itemId);
  }
  const itemList= useMemo( () => {
    console.log("props in item.js",props);

    return (
      <ItemList itemList={props.itemList} onRemoveItem= {removeItemHandler}/>
    )
  }, [props.itemList ,removeItemHandler])

  const addItemHandler = useCallback ( (item) => {
    props.onAddItems(item.name,item.quantity);
  } , [])

    return(
        <div>
           <ItemForm  onAddItem={addItemHandler} loading={false} />

          From Items Component
          {itemList}
        </div>
    )
}

const mapStatetoProps = state => {
  return {
    itemList: state.items.itemList
     
  }
}


const mapDispatchToProps = dispatch => {
  return {
    onAddItems: (name,quantity) => dispatch (actions.addItems(name,quantity)),
    onRemoveItems: (itemId) => dispatch (actions.removeItems(itemId))
  }
}


export default connect(mapStatetoProps,mapDispatchToProps)(Items);