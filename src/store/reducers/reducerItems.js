import * as actionTypes from '../action/actionTypes';
import {updateObject} from '../../shared/utility'

const initialState = {
    itemList: [{id:1,name:"apple",quantity:7},{id:3,name:"lemon",quantity:7}],
    error: false, 
};

const reducer = ( state = initialState, action ) => {
    switch ( action.type ) {
        case actionTypes.ADD_ITEM:
            const newItem = { id:getRandomInt(1000),"name":action.itemName,"quantity":parseInt(action.itemQty) };
            const updatedItems = state.itemList.map(a => ({...a}));
            updatedItems.push(newItem);
            const updatedState = {
                itemList: updatedItems
            }
            return updateObject(state,updatedState);
           // return updatedState; 
        case actionTypes.REMOVE_ITEM:
            var ItemListCopy = state.itemList.map(a => ({...a}));
            ItemListCopy=ItemListCopy.filter(item => item.id !== action.itemId);
            const updatedStateRemove = {
                itemList: ItemListCopy
            }
            return updateObject(state,updatedStateRemove);
        case actionTypes.FETCH_ITEM:
            return state;

        default:
            return state;
    }
}

function getRandomInt(max) {
    return Math.floor(Math.random() * Math.floor(max));
  }

export default reducer;