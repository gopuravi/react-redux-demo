import * as actionTypes from './actionTypes';

export const addItems= (name,quantity) => {
    return {
        type: actionTypes.ADD_ITEM,
        itemName: name,
        itemQty:quantity
    };
};

export const removeItems= (itemId) => {
    return {
        type: actionTypes.REMOVE_ITEM,
        itemId: itemId
    };
};

export const fetchItems= () => {
    return {
        type: actionTypes.FETCH_ITEM,
    };
};