import React, { useEffect } from 'react';

import classes from  './ItemList.module.css';

const ItemList = props => {
  useEffect(()=>{
      console.log("The props is", props);
  },[props])
  return (
    <section className= {classes.itemList}>
      <h2>Loaded Items</h2>
      <ul>
        {props.itemList.map && props.itemList.map(it => (
          <li key={it.id} onClick={props.onRemoveItem.bind(this, it.id)}>
            <span>{it.name}</span>
            <span>{it.quantity}x</span>
          </li>
        ))} 
      </ul>
    </section>
  );
};

export default ItemList;