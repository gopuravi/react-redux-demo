import React, { useState, useEffect} from 'react';
import LoadingIndicator from '../../UI/LoadingIndicator'
import Card from '../../UI/Card';
import './ItemForm.css';
const ItemForm = React.memo(props => {
    const [enteredTitle ,setEnteredTitle ] = useState('');
    const [enteredAmount ,setEnteredAmount] = useState('');


  useEffect(()=>{
    
  })


  const submitHandler = event => {
    event.preventDefault();

    props.onAddItem ({name:enteredTitle,quantity:enteredAmount});
  };

  return (
      <React.Fragment>
    <section className="itemForm">
      <Card>
      <form onSubmit={submitHandler}>
          <div className="form-control">
            <label htmlFor="title">Name</label>
            <input type="text" id="title" value={enteredTitle} 
            onChange={event => {setEnteredTitle(event.target.value)} }/>
          </div>
          <div className="form-control">
            <label htmlFor="amount">Amount</label>
            <input type="number" id="amount" value={enteredAmount} 
            onChange={event => {setEnteredAmount(event.target.value)} }
             />
          </div>
          <div className="ingredient-form__actions">
            <button type="submit">Add Ingredient</button>
            {props.loading && <LoadingIndicator />}
          </div>
        </form>
      </Card> 
    </section>
    </React.Fragment>
  );
});

export default ItemForm;
